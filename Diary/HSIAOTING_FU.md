This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Martin Wu E1000000 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture was great.
* I wish to know more about ImageNet.
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* I don't think exponential growth applies to food production.
* Why doesn't my doctor use an AI.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I knew Python.
* I wish the Python exercise was a bit more challenging.
* I learnt the difference of `print` in Python version 2 and 3.
* I love the [Learn Python the hard way](https://learnpythonthehardway.org/) book and recommend it to everyone. 
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) i
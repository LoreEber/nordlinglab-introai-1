This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Martin Wu E1000000 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* I learned about AI in board games
* The same technology may be used in the future for diagnostics
* I'm curious about how group work will be handled in this online class

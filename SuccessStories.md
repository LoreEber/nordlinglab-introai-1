# List of AI success story topics #
 
Here are the topics covered so far. 

Author: 陸皓邦 Hao-Bang Lu
Date: Mar 18, 2020. (I listed them but it'll change)

-------------------------------2019-------------------------------

1. Ai & Law
2. Melomics
3. Art and style imitation
4. AlphaStar: DeepMind’s Starcraft II A.I.
5. AI, a way to revolutionizes our education
6. AI and Human Resource Management
7. Artificial Intelligence Outperforms Human Data Scientists
8. How would you recommend one out of more than 40 Million products?
9. AI Playing Rubik’s Cube
10. Success stories where an Artificial Intelligence outperform humans
11. Website Design Modifications with AI
12. Artificial Intelligence in molecular design
13. Libratus
14. Robotic reporter
15. AI Physicist: derive the law
16. Which one fits me better?AI Dressing Mirror
17. The Application of AI in Dota2
18. AI in Media
19. AirAsia’s goal
20. Forecasting Future Harvests Using Artificial Intelligence
21. AI in Monument restoration
22. Robocop
23. An AI Program that Composes Pop Music
24. The application of AI in NBA
25. Google Ads using AI for Optimized Campaign: How AI Help Advertiser to Write Their Ads: Rio Ananta Perangin-Angin
26. Ai The film editors’ helper.
27. McCormick collaborate with IBM’s machine
28. AI Success Story : THE FUTURE OF OIL AND GAS, POWERED BY AI AND GPUs
29. AI beats humans in Stanford reading comprehension test
30. Successful story of AI: Netflix

-------------------------------2020-------------------------------

31. AI 'outperforms' doctors diagnosing breast cancer
32. Artificial Intelligence Approach for Identification of Diseases through Gene Mapping
33. Neural Approach to Automated Essay Scoring
34. Successful story of AI: Autopilot of Cars
35. AI Success Story of Siri
36. AI on Satellite Imaging
37. Using AI to Interpret Human Emotions
38. AI diagnose breast cancer
39. Lung Nodule Detection
40. Detection, Segmentation and Recognition of Face and its Features Using Neural Network
41. AI in Forensic Science
42. AI finds Influenza vaccine
43. AI in agriculture (plantix)
